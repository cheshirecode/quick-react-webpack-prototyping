# Basic Webpack + ES6 + React starter

Based off https://github.com/wbkd/yet-another-webpack-es6-starterkit to make a React + Webpack setup for prototyping.

PoC to play around with Spottify artist search API.

- Eslint 'google' + 'react'.
- Reverse proxy on webpack dev server.
- ES6 with experimental Babel.
- React 0.15.0.

### Installation

`npm i` OR `yarn`
### Dev server

```
npm run dev
```

### Build

```
npm run build
```