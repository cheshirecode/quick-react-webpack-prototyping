module.exports = require('./webpack.config-helper')({
  isProduction: false,
  devtool: 'cheap-source-map',
  port: 1337,
});
