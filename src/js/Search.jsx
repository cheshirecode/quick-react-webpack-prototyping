import React from 'react';

const Search = ({
  id = 'search',
  name = 'search',
  placeholder = 'Search...',
  searchTerm = '',
  onChange,
  ...props
}) => <div className="searchWrapper">
  <input id={id} name={name} type="text" placeholder={placeholder}
    value={searchTerm} onChange={onChange} {...props} />
</div>;

Search.propTypes = {
  id: React.PropTypes.string,
  name: React.PropTypes.string,
  placeholder: React.PropTypes.string,
  searchTerm: React.PropTypes.string.isRequired,
  onChange: React.PropTypes.func.isRequired,
};

export default Search;
