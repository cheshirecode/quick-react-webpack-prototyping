import React from 'react';
import Search from './Search';
import Item from './Item';
import debounce from './debounce';

class Main extends React.Component {  //eslint-disable-line
  /**
   */
  constructor() {
    super();
    this.state = {
      searchTerm: '',
      items: [],
      error: '',
    };
  }
  /**
   * [triggerSearch description]
   * @param  {[type]} e [description]
   */
  triggerSearch = (e) => {
    this.setState({
        searchTerm: e.target.value,
      },
      debounce(this.search, 250)
    );
  };

  getQuery = () => {
    const { url, ...params } = this.props.dataSource;
    const { searchTerm } = this.state;
    const combinedParams = {
      ...params,
      q: searchTerm,
    };
    const queryString = Object.keys(combinedParams).reduce(
      (prev, k) => `${prev}&${k}=${combinedParams[k]}`,
      ''
    );
    return `${url}${queryString}`;
  };

  /**
   * [search description]
   */
  search = () => {
    this.setState({
      items: [],
      error: '',
    }, this._search);
  };

  /**
   * [description]
   */
  _search = () => {
    const uri = this.getQuery();
    const { minHeight } = this.props;
    // since this is experimental, we don't really care about polyfilling fetch()
    fetch(uri)
      .then(response => {
        const contentType = response.headers.get('content-type');
        if (contentType && contentType.indexOf('application/json') !== -1) {
          return response.json();
        } else {
          this.setState({
            error: 'Server returns non-JSON',
          });
        }
      })
      .then((json) => {
        if (json) {
          const { artists: { items } } = json;
          const filteredItems = items
            // filter out images that aren't square, and not meeting height criteria
            .map(item => ({
              ...item,
              images: item.images
                .filter(img => img.width === img.height && img.height >= minHeight)
                .sort((a, b) => ({
                  [a.width > b.width]: 1,
                  [a.width === b.width]: 0,
                  [a.width < b.width]: -1,
                }[true])),
            }))
            // filter out those without images (meaning no square images to display)
            .filter(item => item.images.length);
          if (filteredItems.length) {
            this.setState({
              items: filteredItems.map(({ images, name } = {}) => ({
                src: images[0].url,
                name,
              })),
            });
          } else {
            this.setState({
              error: 'No results found. Please refine your search.',
            });
          }
        }
      });
  };
  /**
   * [render description]
   * @return {[type]} [description]
   */
  render() {
    const { triggerSearch } = this;
    const { searchTerm, items, error } = this.state;
    return <div>
      <Search onChange={triggerSearch} searchTerm={searchTerm}
        placeholder="Search artist by name..."/>
      { error && <span className="message error">{error}</span> }
      <div className="grid">
        {
          items.map((item, i) => <Item {...item} key={i} />)
        }
      </div>
    </div>;
  }
}
Main.propTypes = {
  dataSource: React.PropTypes.shape({
    url: React.PropTypes.string.isRequired,
    type: React.PropTypes.string.isRequired,
    limit: React.PropTypes.number.isRequired,
    q: React.PropTypes.string.isRequired,
  }).isRequired,
  minHeight: React.PropTypes.number.isRequired,
};
export default Main;
