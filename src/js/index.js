'use strict';

if (module.hot) {
  module.hot.accept();
}

import 'babel-polyfill';
import '../styles/index.scss';

import React from 'react';
import ReactDOM from 'react-dom';
import Main from './Main';

const QUERY_URL = `https://api.spotify.com/v1/search?`;
const dataSource = {
  url: QUERY_URL,
  type: 'artist',
  limit: 50,
  q: '',
};
const MIN_HEIGHT = 262;
ReactDOM.render(
  <Main dataSource={dataSource} minHeight={MIN_HEIGHT} />,
  document.getElementById('root')
);
