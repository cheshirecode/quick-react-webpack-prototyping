import React from 'react';

const Item = ({ src, name, ...props }) => <div className="item" {...props} >
  <img className="image" src={src} />
  <span className="name" title={name}> {name} </span>
</div>;

Item.propTypes = {
  src: React.PropTypes.string.isRequired,
  name: React.PropTypes.string.isRequired,
};
export default Item;
